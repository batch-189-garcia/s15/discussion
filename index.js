// For outputting data or text into the browser console
console.log('Hello World!')

// Single-line comment
/*
	Multi-line comment
*/

// statement is whole set of commands that will run, statements are made up of set of syntax that will run by the browser. ex. console.log()

// [SYNTAX AND STATEMENTS]
// syntax is single word command, is a code that makes up a statement

// variables - stores data, value can change

// constants - one time value set and cannot be change

// [VARIABLES]
// variables are variables that can be re-assigned
let firstName = 'Bernard';
console.log(firstName);

let lastName = 'Garcia';
console.log(lastName);


// variable can re-assign a new value
firstName = 'Elon';
console.log(firstName);


// const variables are variables that cannot be re-assigned
const colorOfTheSun = 'yellow';
console.log('The color of the sun is ' + colorOfTheSun);


/* Error when re-assigning value to a const
colorOfTheSun = 'red';
console.log(colorOfTheSun);*/

// when declaring a variable, you use a keywork like 'let'
let variableName = 'value';

// when re-assigning a value to a variable, you just need the variable name
variableName = 'New value';

/*

Data Types
Strings - alphanumeric chracters, uses '' or ""
Numbers - 
Boolean - logical value, true or false
Null - absence of a value
Object - array of personal information for different persons, uses { } or [] for other data

string, number and boolean - primitive data types
object - composite data type coz they can hold multiple data or a set of data (array)
null - undefined data type coz its null




*/
// DATA TYPES
// 1. String - Denoted by single OR double quotation marks
let personName = 'Bernard'

// 2. Number - No quotation marks and numerical value
let personAge = 15

// 3. Boolean - only true or false
let hasGirlfriend = false

// 4. Array - Denoted by brackets and can contail multiple values inside
let hobbies = ['Cycling', 'Reading', 'Coding']

// 5. Object - Denoted by curly braces, contains value name/label for each value 
let person ={
	personName : 'Earl Diaz',
	personAge: 15,
	hasGirlfriend: false,
	hobbies: ['Cycling', 'Reading', 'Coding']
}

// 6. Null - A placeholder for future variable re-assignments
let wallet = null

// console.log each of the value
console.log(personName);
console.log(personAge);
console.log(hasGirlfriend);
console.log(hobbies);
console.log(wallet);

// to display single value of an object
console.log(person.personAge)

// display singe value of an object
console.log(hobbies[0]);




